#!/bin/bash
#
# Mar 27, 2021
#
# usage:
#   ./svn_vendor_ingest.sh vendor packagelist
#
# notes:
#   see: http://svnbook.red-bean.com/en/1.7/svn.advanced.vendorbr.html

CURDIR=$PWD

VENDOR=$1
PACKAGE_LIST=$2
exec 3<${PACKAGE_LIST}

# def global vars
REPOS=/Users/dhaffner/Desktop/$VENDOR/ARCHIVE/repos/VLIDORT_SVN
LOCAL=/Users/dhaffner/Desktop/$VENDOR/ARCHIVE/working/VLIDORT_SVN
PACKDIR=/Users/dhaffner/Desktop/$VENDOR/ARCHIVE

echo "*** svn vendor ingest ***"
echo REPOS=$REPOS
echo LOCAL=$LOCAL
echo PACKDIR=$PACKDIR

# create repos and local
mkdir -p repos working

if [ ! -d $REPOS ]
then
    svnadmin create $REPOS
else
    echo "message: $REPOS exists already."
    svn info file:///$REPOS/$VENDOR
    svn list file:///$REPOS/$VENDOR
    exit 1
fi

# first package
read -u 3 PACK
echo loading $PACK
tar xzf $PACK
TAG=${PACK%".tar.gz"}
svn import --quiet $TAG file://$REPOS/$VENDOR/latest -m "importing $PACK drop"
svn copy --quiet file://$REPOS/$VENDOR/latest file://$REPOS/$VENDOR/$TAG -m "tagging $TAG"
svn checkout --quiet file://$REPOS $LOCAL

while read -u 3 PACK
do
    echo loading $PACK
    TAG=${PACK%".tar.gz"}
    cd $LOCAL/$VENDOR/latest
    svn update --quiet
    [ "${PWD##*/}" == "latest" ] && rm -rf *
    tar -zxf $PACKDIR/$PACK -C ./ --strip-components 1
    svn add --force --quiet * 
    svn status | grep "^\!" | sed 's/^\! *//g' | xargs svn rm --quiet
    svn commit --quiet -m "importing $PACK drop"
    svn copy --quiet file://$REPOS/$VENDOR/latest file://$REPOS/$VENDOR/$TAG -m "tagging $TAG"
    tar -zxf $PACKDIR/$PACK -C ./ --strip-components 1
    [ -f ".directory" ] && rm -f .directory
    svn status
done

exec 3>&-
cd $CURDIR
rm -rf working

echo "message: ingest done."
svn info file:///$REPOS/$VENDOR
svn list file:///$REPOS/$VENDOR
